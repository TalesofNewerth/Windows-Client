// (C)2008 S2 Games
// leaf.vsh
// 
// ...
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================
uniform vec4	vColor;

uniform vec3	vSunPositionView;

uniform vec3	vAmbient;
uniform vec3	vSunColor;
uniform vec3	vSunColorSpec;

uniform float	fFogStart;
uniform float	fFogEnd;
uniform float	fFogScale;
uniform float	fFogDensity;
uniform vec3	vFog;

#ifdef CLOUDS
uniform mat4	mCloudProj;
#endif

#if (FOG_OF_WAR == 1)
uniform mat4	mFowProj;
#endif

uniform vec4	vLeafClusters[48];

//=============================================================================
// Varying variables
//=============================================================================
varying vec4	v_vColor;

#if (LIGHTING_QUALITY == 0 || LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
varying vec3	v_vNormal;
#elif (LIGHTING_QUALITY == 3)
varying vec3	v_vDiffLight;
#endif

#if (LIGHTING_QUALITY == 0 || FALLOFF_QUALITY == 0)
varying vec3	v_vPositionView;
#endif

#ifdef CLOUDS
varying vec2	v_vClouds;
#endif

#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0)) || FOG_OF_WAR == 1)
varying vec4	v_vData0;
#endif

//=============================================================================
// Vertex attributes
//=============================================================================
attribute vec2	a_vWind;
attribute vec3	a_vLeaf;

//=============================================================================
// Vertex Shader
//=============================================================================
void main()
{
#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0)) || FOG_OF_WAR == 1)
	v_vData0 = vec4(0.0, 0.0, 0.0, 0.0);
#endif

	vec4 vPosition = gl_Vertex + (vLeafClusters[int(a_vLeaf.x)] * a_vLeaf.y);
	vec3 vNormal = gl_Normal;

	vec3 vPositionView = (gl_ModelViewMatrix * vPosition).xyz;

	gl_Position      = gl_ModelViewProjectionMatrix * vPosition;
	gl_TexCoord[0]   = gl_MultiTexCoord0;
	v_vColor         = vColor;
	
#if (LIGHTING_QUALITY == 0 || LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
	v_vNormal        = normalize(gl_NormalMatrix * vNormal);
#elif (LIGHTING_QUALITY == 3)
	vec3 vLight = vSunPositionView;		
	vec3 vWVNormal = normalize(gl_NormalMatrix * vNormal);

	float fDiffuse = max(dot(vWVNormal, vLight), 0.0);

	v_vDiffLight     =  vSunColor * fDiffuse;
#endif
	
#if (LIGHTING_QUALITY == 0 || FALLOFF_QUALITY == 0)
	v_vPositionView  = vPositionView;
#endif
	
#ifdef CLOUDS
	v_vClouds = (mCloudProj * vPosition).xy;
#endif

#if (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0))
	v_vData0.z = length(vPositionView);
#endif

#if (FOG_QUALITY == 1 && FOG_TYPE != 0)
	#if (FOG_TYPE == 0) // FOG_NONE
	v_vData0.w = 0.0;
	#elif (FOG_TYPE == 1) // FOG_LINEAR
	v_vData0.w = clamp(length(vPositionView) * vFog.x + vFog.y, 0.0, 1.0) * vFog.z;
	#elif (FOG_TYPE == 2) // FOG_EXP2
	v_vData0.w = 1.0 - exp2(-length(vPositionView) * fFogDensity);
	#elif (FOG_TYPE == 3) // FOG_EXP
	v_vData0.w = 1.0 - exp(-length(vPositionView) * fFogDensity);
	#elif (FOG_TYPE == 4) // FOG_HERMITE
	v_vData0.w = smoothstep(fFogStart, fFogEnd, length(vPositionView)) * fFogScale;
	#endif
#endif

#if (FOG_OF_WAR == 1)
	v_vData0.xy = (mFowProj * vPosition).xy;
#endif
}
