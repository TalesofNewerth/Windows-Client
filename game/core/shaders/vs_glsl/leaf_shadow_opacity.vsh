// (C)2008 S2 Games
// leaf_shadow_opacity.vsh
// 
// ...
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================
uniform vec4	vLeafClusters[48];

//=============================================================================
// Varying variables
//=============================================================================
#if (SHADOWMAP_TYPE == 0) // SHADOWMAP_R32F
varying vec2	v_vDepth;
#endif

//=============================================================================
// Vertex attributes
//=============================================================================
attribute vec2	a_vWind;
attribute vec3	a_vLeaf;

//=============================================================================
// Vertex Shader
//=============================================================================
void main()
{
	vec4 vPosition = gl_Vertex + (vLeafClusters[int(a_vLeaf.x)] * a_vLeaf.y);

	gl_Position    = gl_ModelViewProjectionMatrix * vPosition;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	
#if (SHADOWMAP_TYPE == 0) // SHADOWMAP_R32F
	v_vDepth       = gl_Position.zw;
#endif
}
