// (C)2008 S2 Games
// mesh2_shadowed_flat.vsh
// 
// ...
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================
uniform vec4	vColor;

uniform vec3	vSunPositionView;

uniform vec3	vAmbient;
uniform vec3	vSunColor;
uniform vec3	vSunColorSpec;

uniform float	fFogStart;
uniform float	fFogEnd;
uniform float	fFogScale;
uniform float	fFogDensity;
uniform vec3	vFog;

#if (SHADOWS == 1)
uniform mat4	mLightWorldViewProjTex;  // Light's World * View * Projection * Tex
#endif

#if (NUM_BONES > 0)
uniform vec4	vBones[NUM_BONES * 3];
#endif

#ifdef CLOUDS
uniform mat4	mCloudProj;
#endif

#if (FOG_OF_WAR == 1)
uniform mat4	mFowProj;
#endif

//=============================================================================
// Varying variables
//=============================================================================
varying vec4	v_vColor;

#if (LIGHTING_QUALITY == 0 || LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
varying vec3	v_vNormal;
#elif (LIGHTING_QUALITY == 3)
varying vec3	v_vDiffLight;
#endif

#if (LIGHTING_QUALITY == 0 || FALLOFF_QUALITY == 0)
varying vec3	v_vPositionView;
#endif

#if (SHADOWS == 1)
varying vec4	v_vTexcoordLight;
#endif

#ifdef CLOUDS
varying vec2	v_vClouds;
#endif

#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0 || SHADOWS == 1)) || FOG_OF_WAR == 1)
varying vec4	v_vData0;
#endif

//=============================================================================
// Vertex attributes
//=============================================================================
#if (NUM_BONES > 0)
attribute vec4	a_vBoneIndex;
attribute vec4	a_vBoneWeight;
#endif

//=============================================================================
// Vertex Shader
//=============================================================================
void main()
{
#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0 || SHADOWS == 1)) || FOG_OF_WAR == 1)
	v_vData0 = vec4(0.0, 0.0, 0.0, 0.0);
#endif

#if (NUM_BONES > 0)
	vec4 vPosition;
	vec3 vNormal;

	//
	// GPU Skinning
	// Blend bone matrix transforms for this bone
	//
	
	vec4 vBlendIndex = a_vBoneIndex;
	vec4 vBoneWeight = a_vBoneWeight;
	
	mat4	mBlend = mat4(0.0);
	for (int i = 0; i < NUM_BONE_WEIGHTS; ++i)
	{
		mBlend[0] += vBones[int(vBlendIndex[i] + 0.0)] * vBoneWeight[i];
		mBlend[1] += vBones[int(vBlendIndex[i] + 1.0)] * vBoneWeight[i];
		mBlend[2] += vBones[int(vBlendIndex[i] + 2.0)] * vBoneWeight[i];
	}

	mat3 mAxis = mat3(mBlend[0].xyz, mBlend[1].xyz, mBlend[2].xyz);
	vec3 vPos = vec3(mBlend[0].w, mBlend[1].w, mBlend[2].w);
	
	vPosition = vec4(mAxis * gl_Vertex.xyz + vPos, 1.0);
	vNormal = normalize(mAxis * gl_Normal);
#else
	vec4 vPosition = gl_Vertex;
	vec3 vNormal = gl_Normal;
#endif

	vec3 vPositionView = (gl_ModelViewMatrix * vPosition).xyz;

	gl_Position       = gl_ModelViewProjectionMatrix * vPosition;
	gl_TexCoord[0].xy = gl_MultiTexCoord0.xy;
	gl_TexCoord[0].zw = gl_MultiTexCoord1.xy;
	v_vColor          = vColor;
	
#if (LIGHTING_QUALITY == 0 || LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
	v_vNormal        = normalize(gl_NormalMatrix * vNormal);
#elif (LIGHTING_QUALITY == 3)
	vec3 vLight = vSunPositionView;		
	vec3 vWVNormal = normalize(gl_NormalMatrix * vNormal);

	float fDiffuse = max(dot(vWVNormal, vLight), 0.0);

	v_vDiffLight     =  vSunColor * fDiffuse;
#endif
	
#if (LIGHTING_QUALITY == 0 || FALLOFF_QUALITY == 0)
	v_vPositionView  = vPositionView;
#endif
	
#if (SHADOWS == 1)
	v_vTexcoordLight = mLightWorldViewProjTex * vPosition;
#endif

#ifdef CLOUDS
	v_vClouds = (mCloudProj * vPosition).xy;
#endif

#if (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0 || SHADOWS == 1))
	v_vData0.z = length(vPositionView);
#endif

#if (FOG_QUALITY == 1 && FOG_TYPE != 0)
	#if (FOG_TYPE == 0) // FOG_NONE
	v_vData0.w = 0.0;
	#elif (FOG_TYPE == 1) // FOG_LINEAR
	v_vData0.w = clamp(length(vPositionView) * vFog.x + vFog.y, 0.0, 1.0) * vFog.z;
	#elif (FOG_TYPE == 2) // FOG_EXP2
	v_vData0.w = 1.0 - exp2(-length(vPositionView) * fFogDensity);
	#elif (FOG_TYPE == 3) // FOG_EXP
	v_vData0.w = 1.0 - exp(-length(vPositionView) * fFogDensity);
	#elif (FOG_TYPE == 4) // FOG_HERMITE
	v_vData0.w = smoothstep(fFogStart, fFogEnd, length(vPositionView)) * fFogScale;
	#endif
#endif

#if (FOG_OF_WAR == 1)
	v_vData0.xy = (mFowProj * vPosition).xy;
#endif
}
