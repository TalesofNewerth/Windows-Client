// (C)2008 S2 Games
// water_nocolor.vsh
// 
// ...
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================
uniform mat4	mWorld;
uniform mat3	mWorldRotate;

uniform vec4	vColor;

uniform vec3	vCameraPositionWorld;
uniform vec3	vSunPositionWorld;

uniform vec3	vAmbient;
uniform vec3	vSunColor;
uniform vec3	vSunColorSpec;

uniform float	fFogStart;
uniform float	fFogEnd;
uniform float	fFogScale;
uniform float	fFogDensity;
uniform vec3	vFog;

#if (SHADOWS == 1)
uniform mat4	mLightWorldViewProjTex;  // Light's World * View * Projection * Tex
#endif

#if (NUM_BONES > 0)
uniform vec4	vBones[NUM_BONES * 3];
#endif

#ifdef CLOUDS
uniform mat4	mCloudProj;
#endif

#if (FOG_OF_WAR == 1)
uniform mat4	mFowProj;
#endif

uniform float	fTime;
uniform float	fSpeed;

//=============================================================================
// Varying variables
//=============================================================================
varying vec4	v_vColor;

varying vec3	v_vNormal;

#if (LIGHTING_QUALITY == 0)
varying vec3	v_vTangent;
varying vec3	v_vBinormal;
#elif (LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
varying vec3	v_vHalfAngle;
varying vec3	v_vSunLight;
#elif (LIGHTING_QUALITY == 3)
varying vec3	v_vDiffLight;
#endif

varying vec4	v_vPositionWorld;

#if (SHADOWS == 1)
varying vec4	v_vTexcoordLight;
#endif

#ifdef CLOUDS
varying vec2	v_vClouds;
#endif

#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0 || SHADOWS == 1)) || FOG_OF_WAR == 1)
varying vec4	v_vData0;
#endif

//=============================================================================
// Vertex attributes
//=============================================================================
attribute vec3	a_vTangent;

#if (NUM_BONES > 0)
attribute vec4	a_vBoneIndex;
attribute vec4	a_vBoneWeight;
#endif

//=============================================================================
// Vertex Shader
//=============================================================================
void main()
{
#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0 || SHADOWS == 1)) || FOG_OF_WAR == 1)
	v_vData0 = vec4(0.0, 0.0, 0.0, 0.0);
#endif

#if (NUM_BONES > 0)
	vec4 vPosition;
	vec3 vNormal;
	vec3 vTangent;

	//
	// GPU Skinning
	// Blend bone matrix transforms for this bone
	//
	
	vec4 vBlendIndex = a_vBoneIndex;
	vec4 vBoneWeight = a_vBoneWeight;
	
	mat4	mBlend = mat4(0.0);
	for (int i = 0; i < NUM_BONE_WEIGHTS; ++i)
	{
		mBlend[0] += vBones[int(vBlendIndex[i] + 0.0)] * vBoneWeight[i];
		mBlend[1] += vBones[int(vBlendIndex[i] + 1.0)] * vBoneWeight[i];
		mBlend[2] += vBones[int(vBlendIndex[i] + 2.0)] * vBoneWeight[i];
	}

	mat3 mAxis = mat3(mBlend[0].xyz, mBlend[1].xyz, mBlend[2].xyz);
	vec3 vPos = vec3(mBlend[0].w, mBlend[1].w, mBlend[2].w);
	
	vPosition = vec4(mAxis * gl_Vertex.xyz + vPos, 1.0);
	vNormal = normalize(mAxis * gl_Normal);
	vTangent = normalize(mAxis * a_vTangent);
#else
	vec4 vPosition = gl_Vertex;
	vec3 vNormal = gl_Normal;
	vec3 vTangent = a_vTangent;
#endif

	vec4 vPositionWorld = mWorld * vPosition;
	float fCamDistance = length(vCameraPositionWorld - vPositionWorld.xyz);
	vec3 vCamDirection = normalize(vCameraPositionWorld - vPositionWorld.xyz);

	gl_Position      = gl_ModelViewProjectionMatrix * vPosition;
	gl_TexCoord[0]   = gl_MultiTexCoord0 + vec4(fSpeed, fSpeed, 0.0, 0.0) * fTime;
	gl_TexCoord[1]   = gl_MultiTexCoord0 + vec4(-fSpeed, -fSpeed, 0.0, 0.0) * fTime;
	v_vColor         = vColor;
	
	v_vPositionWorld = vPositionWorld;
	
	v_vNormal        = mWorldRotate * vNormal;
	
#if (LIGHTING_QUALITY == 0)
	v_vTangent       = mWorldRotate * vTangent;
	v_vBinormal      = cross(v_vTangent, v_vNormal);
#elif (LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
	vec3 vLight = vSunPositionWorld;
	vec3 vWVNormal = v_vNormal;
	vec3 vWVTangent = mWorldRotate * vTangent;
	vec3 vWVBinormal = cross(vWVTangent, vWVNormal);

	mat3 mRotate = transpose(mat3(vWVTangent, vWVBinormal, vWVNormal));
	
	v_vSunLight      = mRotate * vLight;
	v_vHalfAngle     = mRotate * normalize(vLight + vCamDirection);
#elif (LIGHTING_QUALITY == 3)
	vec3 vLight = vSunPositionWorld;		
	vec3 vWVNormal = v_vNormal;

	float fDiffuse = max(dot(vWVNormal, vLight), 0.0);

	v_vDiffLight     = vSunColor * fDiffuse;
#endif
	
#if (SHADOWS == 1)
	v_vTexcoordLight = mLightWorldViewProjTex * vPosition;
#endif

#ifdef CLOUDS
	v_vClouds = (mCloudProj * vPosition).xy;
#endif

#if (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0 || SHADOWS == 1))
	v_vData0.z = fCamDistance;
#endif

#if (FOG_QUALITY == 1 && FOG_TYPE != 0)
	#if (FOG_TYPE == 0) // FOG_NONE
	v_vData0.w = 0.0;
	#elif (FOG_TYPE == 1) // FOG_LINEAR
	v_vData0.w = clamp(fCamDistance * vFog.x + vFog.y, 0.0, 1.0) * vFog.z;
	#elif (FOG_TYPE == 2) // FOG_EXP2
	v_vData0.w = 1.0 - exp2(-fCamDistance * fFogDensity);
	#elif (FOG_TYPE == 3) // FOG_EXP
	v_vData0.w = 1.0 - exp(-fCamDistance * fFogDensity);
	#elif (FOG_TYPE == 4) // FOG_HERMITE
	v_vData0.w = smoothstep(fFogStart, fFogEnd, fCamDistance) * fFogScale;
	#endif
#endif

#if (FOG_OF_WAR == 1)
	v_vData0.xy = (mFowProj * vPosition).xy;
#endif
}
