// (C)2008 S2 Games
// mesh_depth_opacity.vsh
// 
// ...
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================
#if (NUM_BONES > 0)
uniform vec4	vBones[NUM_BONES * 3];
#endif

//=============================================================================
// Varying variables
//=============================================================================

//=============================================================================
// Vertex attributes
//=============================================================================
attribute vec4	a_vBoneIndex;
attribute vec4	a_vBoneWeight;

//=============================================================================
// Vertex Shader
//=============================================================================
void main()
{
#if (NUM_BONES > 0)
	vec4 vPosition;

	//
	// GPU Skinning
	// Blend bone matrix transforms for this bone
	//
	
	vec4 vBlendIndex = a_vBoneIndex;
	vec4 vBoneWeight = a_vBoneWeight;
	
	mat4	mBlend = mat4(0.0);
	for (int i = 0; i < NUM_BONE_WEIGHTS; ++i)
	{
		mBlend[0] += vBones[int(vBlendIndex[i] + 0.0)] * vBoneWeight[i];
		mBlend[1] += vBones[int(vBlendIndex[i] + 1.0)] * vBoneWeight[i];
		mBlend[2] += vBones[int(vBlendIndex[i] + 2.0)] * vBoneWeight[i];
	}

	mat3 mAxis = mat3(mBlend[0].xyz, mBlend[1].xyz, mBlend[2].xyz);
	vec3 vPos = vec3(mBlend[0].w, mBlend[1].w, mBlend[2].w);
	
	vPosition = vec4(mAxis * gl_Vertex.xyz + vPos, 1.0);
#else
	vec4 vPosition = gl_Vertex;
#endif

	gl_Position    = gl_ModelViewProjectionMatrix * vPosition;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
