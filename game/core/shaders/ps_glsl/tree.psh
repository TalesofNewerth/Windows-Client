// (C)2008 S2 Games
// tree.psh
// 
// ...
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================
uniform vec3	vAmbient;
uniform vec3	vSunColor;

uniform float	fFogStart;
uniform float	fFogEnd;
uniform float	fFogScale;
uniform vec3	vFogColor;
uniform float	fFogDensity;
uniform vec3	vFog;

#if (NUM_POINT_LIGHTS > 0)
uniform vec3	vPointLightPositionView[NUM_POINT_LIGHTS];
uniform vec3	vPointLightColor[NUM_POINT_LIGHTS];
uniform vec2	vPointLightFalloff[NUM_POINT_LIGHTS];
#endif

//=============================================================================
// Varying variables
//=============================================================================
varying vec4	v_vColor;

#if (LIGHTING_QUALITY == 0 || LIGHTING_QUALITY == 1 || LIGHTING_QUALITY == 2)
#elif (LIGHTING_QUALITY == 3)
varying vec3	v_vDiffLight;
#endif

#if (LIGHTING_QUALITY == 0 || FALLOFF_QUALITY == 0)
varying vec3	v_vPositionView;
#endif

#ifdef CLOUDS
varying vec2	v_vClouds;
#endif

#if ((FOG_QUALITY == 1 && FOG_TYPE != 0) || (FALLOFF_QUALITY == 1 && (FOG_TYPE != 0)) || FOG_OF_WAR == 1)
varying vec4	v_vData0;
#endif

//=============================================================================
// Samplers
//=============================================================================
uniform sampler2D	diffuse;

#ifdef CLOUDS
uniform sampler2D 	clouds;
#endif

#if (FOG_OF_WAR == 1)
uniform sampler2D	fogofwar;
#endif

//=============================================================================
// Pixel Shader
//=============================================================================
void main()
{
	vec4 cDiffuseColor = texture2D(diffuse, gl_TexCoord[0].xy) * v_vColor;
	
#if (LIGHTING_QUALITY == 0)
	vec3 vCamDirection = -normalize(v_vPositionView);
#endif

#if (FOG_TYPE != 0)
	#if (FALLOFF_QUALITY == 1)
		float fCamDistance = v_vData0.z;
	#else
		float fCamDistance = length(v_vPositionView);
	#endif
#endif
	
	//
	// Lighting
	//

#if (LIGHTING != 0)

	vec3 vDiffuse = vAmbient;
	
	#if (LIGHTING_QUALITY == 3)
	vDiffuse += v_vDiffLight;
	#else // 0 or 1

		#ifdef CLOUDS
	vec3 cCloudColor = texture2D(clouds, v_vClouds).rgb;

	vDiffuse += vSunColor *  cCloudColor;
		#else
	vDiffuse += vSunColor;
		#endif
		
		#if (NUM_POINT_LIGHTS > 0 && LIGHTING_QUALITY == 0)
	// Point Lights
	for (int i = 0; i < NUM_POINT_LIGHTS; ++i)
	{
		vec3 vDeltaPosition = vPointLightPositionView[i] - v_vPositionView;
		float fDistance = length(vDeltaPosition);
	
		float fAttenuation = 1.0 - clamp(fDistance * vPointLightFalloff[i].x + vPointLightFalloff[i].y, 0.0, 1.0);
		
		vDiffuse += vPointLightColor[i] * fAttenuation;
	}
		#endif

	#endif // LIGHTING_QUALITY

#else // LIGHTING == 0
	vec3 vDiffuse = vec3(1.0, 1.0, 1.0);
#endif

	//
	// Fog
	//

#if (FOG_TYPE != 0) // FOG_NONE
	#if (FOG_QUALITY == 1)
	float fFog = v_vData0.w;
	#else
		#if (FOG_TYPE == 0) // FOG_NONE
	float fFog = 0.0;
		#elif (FOG_TYPE == 1) // FOG_LINEAR
	float fFog = clamp(fCamDistance * vFog.x + vFog.y, 0.0, 1.0) * vFog.z;
		#elif (FOG_TYPE == 2) // FOG_EXP2
	float fFog = 1.0 - exp2(-fCamDistance * fFogDensity);
		#elif (FOG_TYPE == 3) // FOG_EXP
	float fFog = 1.0 - exp(-fCamDistance * fFogDensity);
		#elif (FOG_TYPE == 4) // FOG_HERMITE
	float fFog = smoothstep(fFogStart, fFogEnd, fCamDistance) * fFogScale;
		#endif
	#endif
#else
	float fFog = 0.0;
#endif
	
	//
	// Final
	//

#if (FOG_OF_WAR == 1)
	gl_FragColor.rgb = mix((cDiffuseColor.rgb * vDiffuse) * (texture2D(fogofwar, v_vData0.xy).rgb * 0.667 + 0.333), vFogColor, fFog);
#else
	gl_FragColor.rgb = mix(cDiffuseColor.rgb * vDiffuse, vFogColor, fFog);
#endif

	gl_FragColor.a = cDiffuseColor.a;
}
